// MESSAGES CONSOLE
const message = require('./js/messages');

// token
const jwt = require('./js/token');

// modèles
const Order = require('./models/order.model');
const User = require('./models/user.model');

exports.updateStatus = (data, token) => {
  // sécurité pour la mise à jour du statut de la commande avec le token de l'employé
  if (!jwt.checkSocketToken(token)) throw { code: 401 };
  else
    return Order.findById(data._id)
      .populate({
        path: 'shopping_basket_id',
        populate: [
          {
            path: 'articles.option_ids',
            model: 'options',
          },
          {
            path: 'articles.product_id',
            model: 'products',
            populate: {
              path: 'category_id',
              model: 'categories',
            },
          },
        ],
      })
      .populate('user_id')
      .then(order => {
        if (!order) throw { code: 404 };
        else return order.set({ status: data.status }).save();
      })
      .then(order => {
        // si la mise à jour du statut de la commande devient "préparé", on envoie au backoffice pour qu'elle apparaisse
        // dans les commandes à livrer
        if (order.status === 2) {
          let articles = [];
          // récupération des articles
          order.shopping_basket_id.articles.forEach(article => {
            let options = [];
            // récupération des options
            article.option_ids.forEach(option => {
              options.push({
                name: option.name,
                option_price: option.price,
                _id: option._id,
              });
            });
            //
            articles.push({
              name: article.product_id.name,
              options,
              product_price: article.product_id.price,
              article_price: article.price,
              _id: article.product_id._id,
            });
          });
          //
          return {
            _id: order._id,
            currency: order.currency,
            date: {
              created: order.createdAt,
              updated: order.updatedAt,
            },
            id_order: order.id_order,
            payment_method: order.payment_method,
            articles,
            price: order.price,
            status: order.status,
            payment_method: order.payment_method,
          };
        } else return;
      })
      .catch(error => {
        if (error.code === 404)
          message.error('socket/updateStatus : commande introuvable');
        else if (error.code === 401)
          message.error('socket/updateStatus : non autorisé');
        else message.error('socket/updateStatus : ' + error);
        return;
      });
};

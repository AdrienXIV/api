// models
const Option = require('../../models/option.model');

// messages console
const message = require('../../js/messages');
const capitalizeFirstLetter = require('../../js/functions')
  .capitalizeFirstLetter;

/**
 * Ajouter une option
 * @returns {Response} 201 | 401 | 400 | 500
 */
exports.newOption = (req, res) => {
  Option.findOne({ name: req.body.name })
    .then(option => {
      // si elle existe
      if (option) throw { code: 400 };
      // si elle n'existe pas
      else
        return new Option({
          name: capitalizeFirstLetter(req.body.name),
          price: req.body.price,
        }).save();
    })
    .then(() => {
      res.sendStatus(201);
    })
    .catch(error => {
      if (error.code === 400)
        res.status(400).json({
          error: 'Option déjà existante',
        });
      else {
        message.error('option.admin.controller/newOption : ' + error);
        res.status(500).json({ error });
      }
    });
};

/**
 * Modifier une option
 * @returns {Response} 201 | 401 | 404 | 500
 */
exports.editOption = (req, res) => {
  Option.findById(req.params.id)
    .then(option => {
      if (!option) throw { code: 404 };
      else
        return option
          .set({
            name: capitalizeFirstLetter(req.body.name),
            price: req.body.price,
            currency: req.body.currency,
          })
          .save();
    })
    .then(() => {
      res.sendStatus(201);
    })
    .catch(error => {
      if (error.code === 404)
        res.status(404).json({
          error: 'Option introuvable',
        });
      else {
        message.error('option.admin.controller/editOption : ' + error);
        res.status(500).json({ error });
      }
    });
};

/**
 * Supprimer une option
 * @returns {Response} 200 | 401 | 404 | 500
 */
exports.deleteOption = (req, res) => {
  Option.findById(req.params.id)
    .then(option => {
      if (!option) throw { code: 404 };
      else return option.remove();
    })
    .then(() => {
      res.sendStatus(200);
    })
    .catch(error => {
      if (error.code === 404)
        res.status(404).json({
          error: 'Option introuvable',
        });
      message.error('option.admin.controller/deleteOption : ' + error);
      res.status(500).json({ error });
    });
};

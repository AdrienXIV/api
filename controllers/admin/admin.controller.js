const moment = require('moment');
//  modèles
const User = require('../../models/user.model');

// Crypt.js
const CRYPT = require('../../js/crypt');
const message = require('../../js/messages');

// js/token.js
const jwt = require('../../js/token');

/**
 * Logique pour la route d'inscription d'un employé.
 * @returns {Response} 201 | 400 | 401 | 500
 */
exports.signup = (req, res, next) => {
  // seuls les admin peuvent ajouter des employés
  if (jwt.checkRoleToken(req, res) === 'admin') {
    User.findOne({ email: req.body.email })
      .then(user => {
        // utilisateur déja existant
        if (user) throw { code: 400, message: 'Utilisateur déjà existant' };
        else return CRYPT.crypt(req.body.password);
      })
      .then(hash => {
        return new User({
          email: req.body.email,
          password: hash,
          firstname: req.body.firstname,
          lastname: req.body.lastname,
          role: req.body.role,
          avatar: req.body.avatar,
        }).save();
      })
      .then(() => res.sendStatus(201))
      .catch(error => {
        if (error.code === 400) res.status(400).json({ error: error.message });
        else {
          message.error('admin.controller/signup : ' + error);
          res.status(500).json({ error });
        }
      });
  } else {
    res.sendStatus(401);
  }
};

/**
 * @returns {Response} 200 users Array | 401 | 500
 */
exports.getUsers = (req, res) => {
  // seuls les admin peuvent récupérer les employés avec tous les rôles
  if (jwt.checkRoleToken(req, res) === 'admin')
    User.find({ role: ['admin', 'employee', 'manager'] }, null, {
      sort: { role: 1 },
    })
      .then(users => {
        res.status(200).json(users);
      })
      .catch(error => {
        message.error('admin.controller/getUsers : ' + error);
        res.status(500).json({ error });
      });
  else {
    res.sendStatus(401);
  }
};

/**
 * @returns {Response} 200 objet user | 401 | 404 | 500
 */
exports.getUser = (req, res) => {
  // seuls les admin peuvent récupérer les utilisateurs ET les employés
  if (jwt.checkRoleToken(req, res) === 'admin')
    User.findById(req.params.id)
      .then(user => {
        // si l'utilisateur n'existe pas en BDD
        if (!user)
          res.status(404).json({
            error: 'Utilisateur introuvable.',
          });
        res.status(200).json({
          _id: user._id,
          email: user.email,
          role: user.role,
          firstname: user.firstname,
          lastname: user.lastname,
          avatar: user.avatar,
        });
      })
      .catch(error => {
        message.error('admin.controller/getUser : ' + error);
        res.status(500).json({ error });
      });
  else {
    res.sendStatus(401);
  }
};

/**
 * Modifier des utilisateurs/employés dans le backoffice
 * @returns {Response} 201 | 401 | 404 | 500
 */
exports.editUser = (req, res) => {
  let token = jwt.getUserInfos(req, res);
  // seuls les admin peuvent modifier les utilisateurs (à leur demande) ET les employés
  if (jwt.checkRoleToken(req, res) === 'admin')
    User.findById(req.params.id)
      .then(user => {
        // si l'utilisateur n'existe pas en BDD
        if (!user) throw { code: 404, message: 'Utilisateur introuvable' };
        else
          return user
            .set({
              email: req.body.email,
              firstname: req.body.firstname,
              lastname: req.body.lastname,
              avatar: req.body.avatar,
              // utilisation du token pour avoir les infos de la personne qui modifie
              updated_by: token.email,
            })
            .save();
      })
      .then(() => {
        res.sendStatus(201);
      })
      .catch(error => {
        if (error.code === 404) res.status(404).json({ error: error.message });
        else {
          message.error('admin.controller/editUser : ' + error);
          res.status(500).json({ error });
        }
      });
  else {
    res.sendStatus(401);
  }
};

/**
 * Modifier les mots de passe des utilisateurs/employés dans le backoffice
 * @returns {Response} 201 | 400 | 401 | 404 | 500
 */
exports.editUserPassword = (req, res) => {
  // initialisation de la variable pour récupérer le document en bdd
  let user_document = null;
  // seuls les admin peuvent modifier le mot de passe des utilisateurs (à leur demande) ET les employés
  if (jwt.checkRoleToken(req, res) === 'admin')
    User.findById(req.params.id)
      .then(user => {
        user_document = user;
        // si l'utilisateur n'existe pas en BDD
        if (!user_document)
          throw { code: 404, message: 'Utilisateur introuvable' };
        // vérification de l'ancien mdp
        else return CRYPT.decrypt(req.body.old_password, user.password);
      })
      .then(valid => {
        // s'il ne correspond pas
        if (!valid)
          throw { code: 400, message: 'Ancien mot de passe incorrect' };
        // s'il correspond on peut enregistrer le nouveau dans la BDD
        else return CRYPT.crypt(req.body.new_password);
      })
      .then(hash => {
        return user_document
          .set({
            password: hash,
          })
          .save();
      })
      .then(() => {
        res.sendStatus(201);
      })
      .catch(error => {
        if (error.code === 400) res.status(400).json({ error: error.message });
        else if (error.code === 404)
          res.status(404).json({ error: error.message });
        else {
          message.error('user.controller/editUserPassword : ' + error);
          res.status(500).json({ error });
        }
      });
  else {
    res.sendStatus(401);
  }
};

/**
 * @returns {Response} 200 | 401 | 404 | 500
 */
exports.deleteUser = (req, res) => {
  // seul l'admin peut supprimer un employé dans le backoffice
  if (jwt.checkRoleToken(req, res) === 'admin') {
    User.findById(req.params.id)
      .then(user => {
        if (!user) throw { code: 404 };
        else return user.remove();
      })
      .then(() => {
        res.sendStatus(200);
      })
      .catch(error => {
        if (error.code === 404)
          res.status(404).json({ error: 'Utilisateur introuvable' });
        else {
          message.error('user.controller/deleteUser : ' + error);
          res.status(500).json({ error });
        }
      });
  } else {
    res.sendStatus(401);
  }
};

/**
 * @returns {Response} 200 objet role
 */
exports.checkRole = (req, res) => {
  // envoi du rôle
  res.status(200).json({ role: jwt.checkRoleToken(req, res) });
};

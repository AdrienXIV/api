//  modèles
const User = require('../../models/user.model');
const Order = require('../../models/order.model');

// Crypt.js
const CRYPT = require('../../js/crypt');
const message = require('../../js/messages');

// js/token.js
const jwt = require('../../js/token');

// fonctions
const { sortDate } = require('../../js/functions');

/**
 * Récupérer les commandes
 * @returns {Response} 200 | 401 | 500
 */
exports.getOrders = (req, res) => {
  findOrders(req, res)
    .then(orders => {
      let final_orders = [];
      orders.forEach(order => {
        let articles = [];
        // récupération des articles
        order.shopping_basket_id.articles.forEach(article => {
          let options = [];
          // récupération des options
          article.option_ids.forEach(option => {
            options.push({
              name: option.name,
              option_price: option.price,
              _id: option._id,
            });
          });
          //
          // ajout des infos de chaque article
          articles.push({
            name: article.product_id.name,
            options,
            product_price: article.product_id.price,
            article_price: article.price,
            _id: article.product_id._id,
          });
        });
        //
        // ajout des infos de la chaque commande
        final_orders.push({
          _id: order._id,
          currency: order.currency,
          date: {
            created: order.createdAt,
            updated: order.updatedAt,
          },
          id_order: order.id_order,
          payment_method: order.payment_method,
          articles,
          price: order.price,
          status: order.status,
          payment_method: order.payment_method,
        });
      });
      res.status(200).json(final_orders);
    })
    .catch(error => {
      message.error('order.admin.controller/getOrders : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * Récupérer les commandes payées
 * @returns {Response} 200 | 401 | 500
 */
exports.getOrdersPaid = (req, res) => {
  findOrdersPaid(req, res)
    .then(orders => {
      let final_orders = [];
      orders.forEach(order => {
        let articles = [];
        // récupération des articles
        order.shopping_basket_id.articles.forEach(article => {
          let options = [];
          // récupération des options
          article.option_ids.forEach(option => {
            options.push({
              name: option.name,
              option_price: option.price,
              _id: option._id,
            });
          });
          //
          // ajout des infos de chaque article
          articles.push({
            quantity: article.quantity,
            name: article.product_id.name,
            options,
            product_price: article.product_id.price,
            article_price: article.price,
            _id: article.product_id._id,
          });
        });
        //
        // ajout des infos de chaque commande
        final_orders.push({
          _id: order._id,
          currency: order.currency,
          date: {
            created: order.createdAt,
            updated: order.updatedAt,
          },
          id_order: order.id_order,
          payment_method: order.payment_method,
          articles,
          price: order.price,
          status: order.status,
          payment_method: order.payment_method,
        });
      });
      res.status(200).json(final_orders);
    })
    .catch(error => {
      message.error('order.admin.controller/getOrdersPaid : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * Récupérer les commandes préparées
 */
exports.getOrdersPrepared = (req, res) => {
  findOrdersPrepared(req, res)
    .then(orders => {
      let final_orders = [];
      orders.forEach(order => {
        let articles = [];
        // récupération des articles
        order.shopping_basket_id.articles.forEach(article => {
          let options = [];
          // récupération des options
          article.option_ids.forEach(option => {
            options.push({
              name: option.name,
              option_price: option.price,
              _id: option._id,
            });
          });
          //
          articles.push({
            quantity: article.quantity,
            name: article.product_id.name,
            options,
            product_price: article.product_id.price,
            article_price: article.price,
            _id: article.product_id._id,
          });
        });
        //
        final_orders.push({
          _id: order._id,
          currency: order.currency,
          date: {
            created: order.createdAt,
            updated: order.updatedAt,
          },
          id_order: order.id_order,
          payment_method: order.payment_method,
          articles,
          price: order.price,
          status: order.status,
          payment_method: order.payment_method,
        });
      });
      res.status(200).json(final_orders);
    })
    .catch(error => {
      message.error('order.admin.controller/getOrdersPrepared : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * récupération de la commande (quand le panier est confirmé)
 * @returns {Response} 200 | 401 | 404 | 500
 */
exports.getOrder = (req, res) => {
  findOrder(req, res)
    .then(order => {
      if (!order) res.status(404).json({ error: 'Commande introuvable' });
      else
        res.status(200).json({
          _id: order._id,
          id_order: order.id_order,
          date: {
            created: order.createdAt,
            updated: order.updatedAt,
          },
          total_price: order.price,
          fidelity_points: order.fidelity_points,
          currency: order.currency,
          status: order.status,
          payment_method: order.payment_method,
          user_reference: order.shopping_basket_id.user_reference,
          articles: order.shopping_basket_id.articles,
          places: [],
        });
    })
    .catch(error => {
      message.error('order.admin.controller/getOrder : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * Récupérer l'historique de toutes les commandes livrées
 * @returns {Response} 200 | 401 | 500
 */
exports.getOrderHistoryDelivered = (req, res) => {
  findOrdersDelivered(req, res)
    .then(orders => {
      let final_orders = [];
      orders.forEach(order => {
        let articles = [];
        // récupération des articles
        order.shopping_basket_id.articles.forEach(article => {
          let options = [];
          // récupération des options
          article.option_ids.forEach(option => {
            options.push({
              name: option.name,
              option_price: option.price,
              _id: option._id,
            });
          });
          //
          // ajout des infos de la chaque article
          articles.push({
            name: article.product_id.name,
            options,
            product_price: article.product_id.price,
            article_price: article.price,
            _id: article.product_id._id,
          });
        });
        //
        // ajout des infos de la chaque commande
        final_orders.push({
          _id: order._id,
          currency: order.currency,
          date: {
            created: order.createdAt,
            updated: order.updatedAt,
          },
          id_order: order.id_order,
          payment_method: order.payment_method,
          articles,
          price: order.price,
          status: order.status,
          payment_method: order.payment_method,
        });
      });
      res.status(200).json(final_orders);
    })
    .catch(error => {
      message.error(
        'order.admin.controller/getOrderHistoryDelivered : ' + error,
      );
      res.status(500).json({ error });
    });
};

/**
 * Modifier le statut de la commande
 * @returns {Response} 201 | 401 | 404 | 500
 */
exports.updateOrderStatus = (req, res) => {
  findOrder(req, res)
    .then(order => {
      if (!order)
        throw {
          code: 404,
        };
      // mise à jour du statut de la commande
      else return order.set({ status: req.body.status }).save();
    })
    .then(() => {
      res.sendStatus(201);
    })
    .catch(error => {
      if (error.code === 404)
        res.status(404).json({
          error: 'Commande introuvable',
        });
      else {
        message.error('order.admin.controller/updateOrderStatus : ' + error);
        res.status(500).json({ error });
      }
    });
};

// ######################################################################################
//
// Fonctions pour l'admin du controlleur préparant la requête vers la base de données
// afin d'éviter la répétition de code et d'avoir une meilleure visibilité
//
// ######################################################################################

/**
 * Récupérer les commandes payées afin de les traiter
 * Les commandes non payées ne seront pas affichées
 * @returns {Promise} Order.find()
 */
function findOrders(req, res) {
  if (jwt.checkRoleToken(req, res) === ('admin' || 'manager'))
    // article le plus vieux apparait en premier
    return Order.find({}, null, { sort: { createdAt: 1 } })
      .populate({
        path: 'shopping_basket_id',
        populate: [
          {
            path: 'articles.option_ids',
            model: 'options',
          },
          {
            path: 'articles.product_id',
            model: 'products',
            populate: {
              path: 'category_id',
              model: 'categories',
            },
          },
        ],
      })
      .populate('user_id')
      .where('status')
      .gte(1);
  else return res.sendStatus(401);
}

/**
 * Récupérer une commande (la commande doit être payée)
 * @returns {Promise} Order.find()
 */
function findOrder(req, res) {
  if (jwt.checkRoleToken(req, res) === ('admin' || 'manager'))
    return Order.findById(req.params.id)
      .populate({
        path: 'shopping_basket_id',
        populate: [
          {
            path: 'articles.option_ids',
            model: 'options',
          },
          {
            path: 'articles.product_id',
            model: 'products',
            populate: {
              path: 'category_id',
              model: 'categories',
            },
          },
        ],
      })
      .populate('user_id')
      .where('status')
      .gte(1);
  else return res.sendStatus(401);
}

/**
 * Récupérer les commandes livrées
 * Les commandes livrées sont triées de la livraison moins récente vers la plus récente
 * @returns {Promise} Order.find()
 */
function findOrdersDelivered(req, res) {
  // récupérer l'intervalle pour trier les dates
  const sort = sortDate(req.query.sort);
  if (jwt.checkRoleToken(req, res) === ('admin' || 'manager'))
    return Order.find({
      status: 3,
      updatedAt: {
        $gte: sort.start,
        $lte: sort.end,
      },
    })
      .sort({ updatedAt: -1 })
      .populate({
        path: 'shopping_basket_id',
        populate: [
          {
            path: 'articles.option_ids',
            model: 'options',
          },
          {
            path: 'articles.product_id',
            model: 'products',
            populate: {
              path: 'category_id',
              model: 'categories',
            },
          },
        ],
      })
      .populate('user_id');
  else return res.sendStatus(401);
}

/**
 * Récupérer les commandes payées
 * Les commandes préparées sont triées de la moins récente vers la plus récente
 * @returns {Promise} Order.find()
 */
function findOrdersPaid(req, res) {
  if (jwt.checkRoleToken(req, res) === ('admin' || 'manager'))
    return Order.find({}, null, { sort: { createdAt: 1 } })
      .where('status')
      .equals(1)
      .populate({
        path: 'shopping_basket_id',
        populate: [
          {
            path: 'articles.option_ids',
            model: 'options',
          },
          {
            path: 'articles.product_id',
            model: 'products',
            populate: {
              path: 'category_id',
              model: 'categories',
            },
          },
        ],
      })
      .populate('user_id');
  else return res.sendStatus(401);
}

/**
 * Récupérer les commandes préparées
 * Les commandes préparées sont triées de la préparation la moins récente vers la plus récente
 * @returns {Promise} Order.find()
 */
function findOrdersPrepared(req, res) {
  if (jwt.checkRoleToken(req, res) === ('admin' || 'manager'))
    return Order.find({}, null, { sort: { updatedAt: 1 } })
      .where('status')
      .equals(2)
      .populate({
        path: 'shopping_basket_id',
        populate: [
          {
            path: 'articles.option_ids',
            model: 'options',
          },
          {
            path: 'articles.product_id',
            model: 'products',
            populate: {
              path: 'category_id',
              model: 'categories',
            },
          },
        ],
      })
      .populate('user_id');
  else return res.sendStatus(401);
}

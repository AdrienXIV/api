const moment = require('moment');
// user model
const User = require('../models/user.model');
const Reservation = require('../models/reservation.model');

// Crypt.js
const CRYPT = require('../js/crypt');
const jwt = require('../js/token');
const message = require('../js/messages');

const { capitalizeFirstLetter } = require('../js/functions');

const verifyEmployee = ['admin', 'employee', 'manager'];

exports.getDayReservation = (req, res) => {
  Reservation.find({
    location_id: req.params.id,
    day: req.query.day,
    month: req.query.month,
    year: req.query.year,
  })
    .then(places => {
      res.status(200).json(places);
    })
    .catch(error => {
      message.error('reservation.controller/getDayReservation : ' + error);
      res.status(500).json({
        error: `Erreur leur de la récupération des réservations pour la semaine ${req.params.week}`,
      });
    });
};

exports.newHourReservation = data => {
  return Reservation.findOne(data)
    .then(reservation => {
      if (reservation) throw { code: 400 };
      else {
        return new Reservation(data).save();
      }
    })
    .then(reservation => {
      return reservation;
    })
    .catch(error => {
      return error.code === 400 ? { code: 400 } : { code: 500 };
    });
};

exports.removeHourReservation = id => {
  return Reservation.findById(id).then(reservation => {
    if (reservation) return reservation.remove();
    else
      throw {
        code: 404,
        error: 'Emplacement réservé pour cette heure introuvable',
      };
  });
};

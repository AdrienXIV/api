// models
const Category = require('../models/category.model');

const jwt = require('jsonwebtoken');
// messages console
const message = require('../js/messages');
/**
 * Récupérer toutes les catégories
 * @returns {Response} 200 | 500
 */
exports.getCategories = (req, res) => {
  Category.find()
    .sort({ name: 1 })
    .then(Categories => {
      res.status(200).json(Categories);
    })
    .catch(error => {
      message.error('category.controller/getCategories : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * Récupérer une catégorie
 * @returns {Response} 200 | 500
 */
exports.getCategory = (req, res) => {
  Category.findById(req.params.id)
    .then(Category => {
      res.status(200).json(Category);
    })
    .catch(error => {
      message.error('category.controller/getCategory : ' + error);
      res.status(500).json({ error });
    });
};

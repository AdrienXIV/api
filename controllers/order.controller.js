require('dotenv').config();
const randomstring = require('randomstring');
const moment = require('moment');
const Stripe = require('stripe');
const stripe = new Stripe(process.env.STRIPE_SECRET_KEY);

// fonctions
const jwt = require('../js/token');
const {
  createRandomString,
  findBasket,
  findBasketWithUserId,
  findOrder,
  findUserOrders,
  updateFidelityPoints,
  sendMailWhenOrderIsPaid,
} = require('../js/functions');

// modèles
const Order = require('../models/order.model');
const ShoppingBasket = require('../models/shopping_basket.model');
const User = require('../models/user.model');

// messages console
const message = require('../js/messages');

/**
 * Client
 * récupération de la commande
 * @returns {Response} 200 | 404 | 500
 */
exports.getOrder = (req, res) => {
  findOrder(req.params.id)
    .then(order => {
      if (!order) res.status(404).json({ error: 'Commande introuvable' });
      else res.status(200).json(order);
    })
    .catch(error => {
      message.error('order.controller/getOrder : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * Client
 * récupération des commandes de l'utilisateur
 * @returns {Response} 200 | 401 | 500
 */
exports.getUserOrders = (req, res) => {
  if (req.headers.authorization) {
    const { userId } = jwt.getUserInfos(req, res);
    findUserOrders(userId)
      .then(orders => {
        res.status(200).json(orders);
      })
      .catch(error => {
        message.error('order.controller/getUserOrders : ' + error);
        res.status(500).json({ error });
      });
  } else res.sendStatus(401);
};

/**
 * si le paiement a été confirmé (banque, paypal, etc)
 * on met à jour la variable status dans order.model
 * si l'utilisateur est connecté, ses points de fidélité seront mis à jour
 * @returns {Response} 201 | 404 | 500
 */
exports.payment = async (req, res) => {
  const id_stripe = req.body.id_stripe;
  const _id = req.body._id;
  let user = null;

  try {
  } catch (error) {}
  try {
    const basket = await findBasket(_id);
    if (!basket) throw { code: 404 };

    // 10,00 euros => 1000 centimes pour stripe
    // doit être > 50 centimes sinon erreur stripe
    const amount =
      Number((basket.total_price * 100).toFixed(2)) > 50
        ? Number((basket.total_price * 100).toFixed(2))
        : 51;

    // si l'utilisateur dispose d'un compte client alors il a un token dans le header de ses requêtes
    if (req.headers.authorization) {
      user = jwt.getUserInfos(req, res);
      if (!user) return res.status(403).json({ error: 'Non autorisé' });
      updateFidelityPoints(user.userId, basket.preview_fidelity_points);
    }

    // try catch pour identifier l'erreur de stripe si le paiement echoue
    try {
      // paiement
      await stripe.paymentIntents.create({
        amount,
        currency: 'EUR',
        description: 'gusto coffee',
        payment_method: id_stripe,
        confirm: true,
      });
    } catch (error) {
      console.log('error: ', error);
      return res
        .status(400)
        .json({ error: 'Problème survenu lors du paiement' });
    }

    // ajout de la nouvelle commande payée
    const order = await new Order({
      status: 1, // mise à jour de la commande, 1 = payée
      currency: req.body.currency,
      payment_method: req.body.payment_method,
      shopping_basket_id: basket._id,
      price: basket.total_price,
      fidelity_points: basket.preview_fidelity_points,
      id_order: createRandomString(),
      user_id: user ? user.userId : null,
    }).save();

    // si l'utilisateur dispose d'un mail
    if (user !== null) sendMailWhenOrderIsPaid(order._id, user.email);

    res.status(201).json({
      _id: order._id,
      message: `Commande ${order.id_order} acceptée`,
    });
  } catch (error) {
    if (error.code === 404)
      res.status(404).json({ errror: 'Commande introuvable' });
    else {
      message.error('order.controller/payment : ' + error);
      res.status(500).json({ error: 'Erreur serveur' });
    }
  }
};

// stripe mobile
exports.mobileCreatePayment = async (req, res) => {
  const card = req.body.card;

  if (!card.number || !card.exp_month || !card.exp_year || !card.cvc) {
    return res
      .status(409)
      .json({ error: 'Veuillez renseigner une carte de crédit' });
  }
  try {
    const paymentMethod = await stripe.paymentMethods.create({
      type: 'card',
      card,
    });

    res.status(200).json({ id: paymentMethod.id });
  } catch (error) {
    console.log('error: ', error);
    res
      .status(500)
      .json({ error: 'Veuillez renseigner une carte de crédit valide' });
  }
};

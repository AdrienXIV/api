// modèles
const LocationCategory = require('../models/location_category.model');
const Location = require('../models/location.model');

const jwt = require('jsonwebtoken');
// messages console
const message = require('../js/messages');

/**
 * Récupérer toutes les catégories d'emplacements
 * @returns {Response} 200 | 500
 */
exports.getLocationCategories = (req, res) => {
  LocationCategory.find()
    .sort({ name: 1 })
    .then(location_categories => {
      res.status(200).json(location_categories);
    })
    .catch(error => {
      message.error('coworking.controller/getLocationCategories : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * Récupérer une catégorie d'emplacement
 * @returns {Response} 200 | 404 | 500
 */
exports.getLocationCategory = (req, res) => {
  LocationCategory.findById(req.params.id)
    .then(location_category => {
      if (!location_category)
        res.status(404).json({ error: "Catégorie d'emplacement introuvable" });
      else res.status(200).json(location_category);
    })
    .catch(error => {
      message.error('coworking.controller/getLocationCategory : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * Récupérer tous les emplacement
 * @returns {Response} 200 | 500
 */
exports.getLocations = (req, res) => {
  Location.find()
    .sort({ name: 1 })
    .populate('location_category_id')
    .then(locations => {
      res.status(200).json(locations);
    })
    .catch(error => {
      message.error('coworking.controller/getLocations : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * Récupérer une catégorie d'emplacement
 * @returns {Response} 200 | 404 | 500
 */
exports.getLocation = (req, res) => {
  Location.findById(req.params.id)
    .populate('location_category_id')
    .then(location => {
      if (!location) res.status(404).json({ error: 'Emplacement introuvable' });
      else res.status(200).json(location);
    })
    .catch(error => {
      message.error('coworking.controller/getLocation : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * Récupérer tous les emplacements d'une catégorie
 * @returns {Response} 200 | 500
 */
exports.getCategoryLocations = (req, res) => {
  Location.find({ location_category_id: req.params.id })
    .populate('location_category_id')
    .then(locations => {
      res.status(200).json(locations);
    })
    .catch(error => {
      message.error('coworking.controller/getCategoryLocations : ' + error);
      res.status(500).json({ error });
    });
};

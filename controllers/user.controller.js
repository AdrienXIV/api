require('dotenv').config();
const moment = require('moment');
const randomstring = require('randomstring');
// user model
const User = require('../models/user.model');
// Crypt.js
const CRYPT = require('../js/crypt');
const jwt = require('../js/token');
const message = require('../js/messages');

const {
  capitalizeFirstLetter,
  sendMailWhenUserIsRegistered,
  sendMailWhenUserForgotHisPassword,
} = require('../js/functions');

const verifyEmployee = ['admin', 'employee', 'manager'];

// cache
const NodeCache = require('node-cache');
const { vide, isEmail, goodPassword } = require('../js/verifs');
const myCache = new NodeCache();

/**
 * Logique pour la route d'inscription.
 * @returns {Response} 201 | 400 | 409 | 500
 */
exports.signup = (req, res) => {
  const errors = [];
  // vérif des champs vides
  if (vide(req.body.firstname)) errors.push('Prénom manquant');
  if (vide(req.body.lastname)) errors.push('Nom manquant');

  // vérif de l'email
  if (vide(req.body.email)) errors.push('Courriel manquant');
  // pas besoin de faire la vérif si l'email est vide car elle est faite juste au desssus
  if (!vide(req.body.email) && !isEmail(req.body.email))
    errors.push('Format inccorect pour le courriel');

  // vérif du mdp
  if (vide(req.body.password)) errors.push('Mot de passe manquant');
  if (!vide(req.body.password) && !goodPassword(req.body.password))
    errors.push('Format incorrect pour le mot de passe');

  // si y'a des erreurs
  if (errors.length > 0) {
    return res.status(409).json({ error: errors.join(' | ') });
  }

  User.findOne({ email: req.body.email })
    .then(user => {
      if (user) throw { code: 400, message: 'Courriel déjà existant' };
      else return CRYPT.crypt(req.body.password);
    })
    .then(hash => {
      return new User({
        email: req.body.email,
        password: hash,
        firstname: capitalizeFirstLetter(req.body.firstname),
        lastname: capitalizeFirstLetter(req.body.lastname),
        avatar: req.body.avatar,
      }).save();
    })
    .then(user => {
      // envoi du mail à l'utilisateur
      sendMailWhenUserIsRegistered(user.email, user.firstname, user.lastname);
      res.status(201).json({
        user: {
          id: user._id,
          role: user.role,
          firstname: user.firstname,
          lastname: user.lastname,
          email: user.email,
          avatar: user.avatar,
          token: jwt.createToken(
            user._id,
            user.role,
            user.firstname,
            user.lastname,
            user.email,
          ),
        },
      });
    })
    .catch(error => {
      console.log('error: ', error);
      if (error.code) res.status(error.code).json({ error: error.message });
      else {
        message.error('user.controller/signup : ' + error);
        res.status(500).json({ error });
      }
    });
};

/**
 * Logique pour la route de connexion.
 * @returns {Response} 200 objet user contenant le token | 400 | 404 | 500
 */
exports.login = (req, res) => {
  if (vide(req.body.email))
    return res.status(409).json({ error: 'Veuillez renseigner un courriel' });

  // informations de l'utilisateur à envoyer dans la réponse au client
  let user_infos = {};
  // On cherche l'utilisateur par rapport à son email
  User.findOne({ email: req.body.email })
    .then(user => {
      // S'il n'existe pas
      if (!user) throw { code: 404 };
      // Sinon on compare le mot de passe entre au hash en db
      else {
        user_infos = {
          id: user._id,
          role: user.role,
          firstname: user.firstname,
          lastname: user.lastname,
          email: user.email,
          fidelity_points: user.fidelity_points,
          avatar: user.avatar,
          token: jwt.createToken(
            user._id,
            user.role,
            user.firstname,
            user.lastname,
            user.email,
          ),
        };
        return CRYPT.decrypt(req.body.password, user.password);
      }
    })
    .then(valid => {
      // Si mdp invalide
      if (!valid)
        res.status(400).json({
          error: 'Couple adresse mail / mot de passe invalide.',
        });
      // Si mdp valide on envoie un token valide pendant 24h + les infos de l'utilisateur
      else res.status(200).json({ user: user_infos });
    })
    .catch(error => {
      // si l'utilisateur n'existe pas (throw au début)
      if (error.code === 404)
        res.status(404).json({ error: 'Courriel invalide' });
      else {
        message.error('user.controller/login : ' + error);
        res.status(500).json({ error });
      }
    });
};
// récupérer un mail pour avoir le lien afin de pour réinitialiser son mdp oublié
exports.forgotPassword = (req, res) => {
  User.findOne({ email: req.body.email })
    .then(user => {
      if (!user) res.status(404).json({ error: 'Profil introuvable' });
      else {
        const id = randomstring.generate({
          length: 48,
          charset: 'alphanumeric',
        });
        const link = `${process.env.URL}/reinitialisation-de-mon-mot-de-passe/${id}`;

        // stockage des infos en cache afin de changer son mot de passe oublié plus tard
        myCache.set(String(id), req.body.email, 900); // 15min

        sendMailWhenUserForgotHisPassword(req.body.email, link, res);
      }
    })
    .catch(error => {
      message.error('user.controller/forgotPassword : ' + error);
      res.status(500).json({ error });
    });
};
// logique pour réinitialiser son mdp oublié
exports.resetPassword = (req, res) => {
  let email = myCache.get(String(req.body.id));
  // si la valeur enregistré en cache n'existe plus (utilisateur a mit trop de temps à changer son mdp)
  if (email === undefined)
    res.status(400).json({ error: 'Expiration du délai, veuillez réessayer' });
  else {
    let user_document = null;
    User.findOne({ email })
      .then(user => {
        if (!user) throw { code: 404 };
        else {
          user_document = user;
          // hash mdp
          return CRYPT.crypt(req.body.password);
        }
      })
      .then(hash => {
        // mise à jour du mdp
        return user_document.set({ password: hash }).save();
      })
      .then(() => {
        // supprimer la valeur du cache
        myCache.del(String(req.body.id));
        res.sendStatus(201);
      })
      .catch(error => {
        if (error.code === 404)
          res.status(404).json({ error: 'Profil inexistant' });
        else {
          message.error('user.controller/resetPassword : ' + error);
          res.status(500).json({ error });
        }
      });
  }
};

exports.getProfile = (req, res) => {
  if (req.headers.authorization) {
    const { userId } = jwt.getUserInfos(req, res);
    User.findById(userId)
      .then(user => {
        if (!user) res.status(404).json({ error: 'Profil introuvable' });
        const data = JSON.parse(JSON.stringify(user));
        delete data['password'];
        res.status(200).json(data);
      })
      .catch(error => {
        message.error('user.controller/getProfile : ' + error);
        res.status(500).json({ error });
      });
  } else res.sendStatus(401);
};

/**
 * Récupérer tous les utilisateurs
 * @returns {Response}  200 users Array | 500
 */
exports.getUsers = (req, res) => {
  User.find()
    .sort({ name: 1 })
    .then(users => {
      res.status(200).json(users);
    })
    .catch(error => {
      message.error('user.controller/getUsers : ' + error);
      res.status(500).json({ error });
    });
};

/**
 * @returns {Response} 201 objet user contenant un nouveau token | 400 | 401 | 404 | 500
 */
exports.editUser = (req, res) => {
  let oldUserDocument = null;
  const token = jwt.getUserInfos(req, res);
  User.findById(token.userId)
    .then(user => {
      // sauvegarde de l'utilisateur trouvé avec son ID si jamais on modifie son courriel
      oldUserDocument = user;

      if (!user) throw { code: 404 };
      // vérifier que l'utilisateur modifie bien son profil
      else if (user.email !== token.email) throw { code: 401 };
      else {
        // l'utilisateur change son courriel
        // si le courriel est différent de son courriel actuel, on vérifie qu'il n'existe pas en BDD
        return User.findOne({ email: req.body.email });
      }
    })
    .then(user => {
      // si l'utilisateur change de courriel et qu'il n'est pas utilisé
      if (!user) {
        // utilisation du document utilisateur trouvé avec son ID
        return oldUserDocument
          .set({
            email: req.body.email,
            firstname: capitalizeFirstLetter(req.body.firstname),
            lastname: capitalizeFirstLetter(req.body.lastname),
            avatar: req.body.avatar,
            // utilisation du token pour avoir les infos de la personne qui modifie
            updated_by: token.email,
          })
          .save();
      }
      // si l'utilisateur ne change pas de courriel
      else if (user.email === token.email) {
        return user
          .set({
            firstname: capitalizeFirstLetter(req.body.firstname),
            lastname: capitalizeFirstLetter(req.body.lastname),
            avatar: req.body.avatar,
            // utilisation du token pour avoir les infos de la personne qui modifie
            updated_by: token.email,
          })
          .save();
      } else {
        // si le courriel est déjà utilisé
        throw { code: 400 };
      }
    })
    .then(user => {
      res.status(201).json({
        user: {
          id: user._id,
          role: user.role,
          firstname: user.firstname,
          lastname: user.lastname,
          email: user.email,
          avatar: user.avatar,
          token: jwt.createToken(
            user._id,
            user.role,
            user.firstname,
            user.lastname,
            user.email,
          ),
        },
      });
    })
    .catch(error => {
      if (error.code === 400)
        res.status(400).json({ error: 'Courriel déjà existant' });
      else if (error.code === 401) res.sendStatus(401);
      else if (error.code === 404)
        res.status(404).json({ error: 'Utilisateur introuvable' });
      else {
        message.error('user.controller/editUser : ' + error);
        res.status(500).json({ error });
      }
    });
};

/**
 * Modifier les mots de passe des utilisateurs
 * @returns {Response} 201 | 400 | 401 | 404 | 500
 */
exports.editUserPassword = (req, res) => {
  // initialisation de la variable pour récupérer le document en bdd
  let user_document = null;
  let token = jwt.getUserInfos(req, res);
  User.findById(token.userId)
    .then(user => {
      // si l'utilisateur n'existe pas en BDD
      if (!user) throw { code: 404, message: 'Utilisateur introuvable' };
      // vérifier que l'utilisateur modifie bien son compte
      else if (user.email !== token.email) throw { code: 401 };
      else {
        user_document = user;
        // vérification de l'ancien mdp
        return CRYPT.decrypt(req.body.old_password, user.password);
      }
    })
    .then(valid => {
      // s'il ne correspond pas
      if (!valid) throw { code: 400, message: 'Ancien mot de passe incorrect' };
      // s'il correspond on peut enregistrer le nouveau dans la BDD
      else return CRYPT.crypt(req.body.new_password);
    })
    .then(hash => {
      return user_document
        .set({
          password: hash,
        })
        .save();
    })
    .then(() => {
      res.sendStatus(201);
    })
    .catch(error => {
      if (error.code === 400) res.status(400).json({ error: error.message });
      else if (error.code === 401) res.sendStatus(401);
      else if (error.code === 404)
        res.status(404).json({ error: error.message });
      else {
        message.error('user.controller/editUserPassword : ' + error);
        res.status(500).json({ error });
      }
    });
};

/**
 * Utilisateur qui décide de supprimer son comtpe
 * @returns {Response} 200 | 401 | 404 | 500
 */
exports.deleteUser = (req, res) => {
  const { userId } = jwt.getUserInfos(req, res);

  User.findById(userId)
    .then(user => {
      if (!user) throw { code: 404 };
      // sécurité pour ne pas supprimer les employées sur la route utilisateur
      else if (verifyEmployee.indexOf(user.role) >= 0) throw { code: 401 };
      else return user.remove();
    })
    .then(() => {
      res.sendStatus(200);
    })
    .catch(error => {
      if (error.code === 401) res.sendStatus(401);
      else if (error.code === 404)
        res.status(404).json({
          error: 'Utilisateur introuvable',
        });
      else {
        message.error('user.controller/deleteUser : ' + error);
        res.status(500).json({ error });
      }
    });
};

require('dotenv').config();
require('./database');
require('./js/sendMail');

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors');
const http = require('http').createServer(app);
const session = require('express-session');
const cookieParser = require('cookie-parser');
const helmet = require('helmet');
const TDD = require('./TDD');

// MESSAGES CONSOLE
const message = require('./js/messages');

// FONCTIONS
const { findOrder, newslettersSignup } = require('./js/functions');

// PORT
const __PORT = Number(process.env.PORT) || 5000;

// expiration cookie
var expiryDate = new Date(Date.now() + 60 * 60 * 24 * 1000); // 24 heures

// check token
const jwt = require('./js/token');

// controlleurs
const orderAdminController = require('./controllers/admin/order.admin.controller');
const socketController = require('./socket');
const reservationController = require('./controllers/reservation.controller');

/**
 * ROUTES
 */
const routes = {
  user: require('./routes/user.route'),
  auth: require('./routes/auth.route'),
  admin: require('./routes/admin/admin.route'),
  product_admin: require('./routes/admin/product.admin.route'),
  category_admin: require('./routes/admin/category.admin.route'),
  option_admin: require('./routes/admin/option.admin.route'),
  coworking_admin: require('./routes/admin/coworking.admin.route'),
  order_admin: require('./routes/admin/order.admin.route'),
  product: require('./routes/product.route'),
  category: require('./routes/category.route'),
  option: require('./routes/option.route'),
  booking: require('./routes/booking.route'),
  order: require('./routes/order.route'),
  shopping_basket: require('./routes/shopping_basket.route'),
  coworking: require('./routes/coworking.route'),
  reservation: require('./routes/reservation.route'),
};

/**
 * Middleware
 */
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.use(helmet()); // headers
app.use(cookieParser('je suis un secret secret'));
app.set('trust proxy', 1); // trust first proxy
app.use(
  session({
    secret: 'je suis un secret secret',
    resave: false,
    saveUninitialized: true,
    cookie: {
      httpOnly: true,
      secure: true,
      expires: expiryDate,
      sameSite: 'strict',
      path: '/',
    },
  }),
);
app.use('/', function (req, res, next) {
  res.set({
    'Access-Control-Allow-Methods': 'POST, GET, PATCH, DELETE, OPTIONS',
    'Access-Control-Allow-Origin': '*',
  });

  next();
});

// newsletter
app.post('/newsletter', newslettersSignup);

// token obligatoire
app.use('/user', jwt.checkUserToken, routes.user);
// route login, signup
app.use('/auth', routes.auth);

// Routes application front
app.use('/product', routes.product);
app.use('/category', routes.category);
app.use('/option', routes.option);
app.use('/booking', routes.booking);
app.use('/order', routes.order);
app.use('/shopping-basket', routes.shopping_basket);
app.use('/coworking', routes.coworking);
app.use('/reservation', routes.reservation);

// route backoffice
app.use('/admin', jwt.checkEmployeeToken, routes.admin);
app.use('/admin/product', jwt.checkEmployeeToken, routes.product_admin); // gérer les produits
app.use('/admin/category', jwt.checkEmployeeToken, routes.category_admin); // gérer les catégories des produits
app.use('/admin/option', jwt.checkEmployeeToken, routes.option_admin); // gérer les options des produits
app.use('/admin/coworking', jwt.checkEmployeeToken, routes.coworking_admin); // gérer les espaces de coworking
app.use('/admin/order', jwt.checkEmployeeToken, routes.order_admin); // gérer les commandes

/**
 * LANCEMENT SERVEUR + BDD
 */
try {
  // si le serveur est connecté on appelle la connexion à la BDD
  http.listen(__PORT);
  TDD.assert(`Serveur connexion port ${__PORT}`, true);

  // socketio
  //require('./socket');
} catch (error) {
  message.error(error);
  TDD.assert(`Serveur connexion port ${__PORT}`, false);
}

/**
 * SOCKET IO
 */
const io = require('socket.io')(http);
var room = '';
var token = '';

io.on('connection', socket => {
  room = socket.handshake.query.room;
  token = socket.handshake.query.token;

  // salon correspondant au lieu de l'entreprise
  // ici, ce salon correspondra à gusto coffee de Paris
  socket.join(room);

  // récupération des commandes de l'utilisateur pour les envoyer au backoffice
  socket.on('new-order', ({ _id }) => {
    findOrder(_id)
      .then(order => {
        if (order) {
          let articles = [];
          let final_order = {};
          // récupération des articles
          order.shopping_basket_id.articles.forEach(article => {
            let options = [];
            // récupération des options
            article.option_ids.forEach(option => {
              options.push({
                name: option.name,
                option_price: option.price,
                _id: option._id,
              });
            });
            //
            articles.push({
              quantity: article.quantity,
              name: article.product_id.name,
              options,
              product_price: article.product_id.price,
              article_price: article.price,
              _id: article.product_id._id,
            });
          });
          //
          final_order = {
            _id: order._id,
            currency: order.currency,
            date: {
              created: order.createdAt,
              updated: order.updatedAt,
            },
            id_order: order.id_order,
            payment_method: order.payment_method,
            articles,
            price: order.price,
            status: order.status,
            payment_method: order.payment_method,
          };

          io.to(room).emit('orders-paid', final_order);
        }
      })
      .catch(error => {
        message.error('index/socket.on(new-order) : ' + error);
        return;
      });
  });

  // mise à jour du statut de la commande
  socket.on('update-order-status', (data, response) => {
    socketController.updateStatus(data, token).then(order => {
      // si la commande passe de payée à préparée
      // on envoie les infos au backoffice + confirmation de mise à jour
      // sinon on envoie seulement la confirmation de mise à jour
      if (order) io.to(room).emit('orders-prepared', order);
      // envoie au client la confirmation de la mise à jour du statut
      response(201);
    });
  });

  socket.on('new-hour-reservation', (data, response) => {
    const { year, month, day, start } = data;

    if (
      new Date(`${year}-${month}-${day} ${start + 1}:00:00`).getTime() <=
      Date.now()
    ) {
      return response({
        code: 400,
        error: 'Veuillez réserver une plage horaire valide',
      });
    }
    reservationController
      .newHourReservation(data)
      .then(reservation => {
        response({ code: 201, data: reservation });
        io.to(room).emit('hours-reservation', 'ping');
      })
      .catch(error => {
        if (error.code === 400)
          response({ code: 400, error: 'Emplacement indisponible' });
        else {
          message.error('index/socket.on(new-hour-reservation) : ' + error);
          response({ code: 500, error: 'Problème avec le serveur' });
        }
      });
  });

  socket.on('remove-hour-reservation', ({ _id }, response) => {
    reservationController
      .removeHourReservation(_id)
      .then(() => {
        response({ code: 200 });
        io.to(room).emit('hours-reservation', 'ping');
      })
      .catch(error => {
        if (error.code === 404) response({ code: 404, error: error.message });
        else {
          message.error('index/socket.on(remove-hour-reservation) : ' + error);
          response({
            code: 500,
            error: 'Problème avec le serveur',
          });
        }
      });
  });

  // fin socket io
});

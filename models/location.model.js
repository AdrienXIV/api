const mongoose = require('mongoose');
const moment = require('moment');
const type = mongoose.Types;

// Schéma
const LocationSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Nom de l'emplacement manquant."],
    },
    location_category_id: { type: type.ObjectId, ref: 'location_categories' },
  },
  { timestamps: true },
);

// Modèle
module.exports = mongoose.model('locations', LocationSchema);

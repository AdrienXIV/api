const mongoose = require('mongoose');
const type = mongoose.Types;
const moment = require('moment');

// Schéma
const reservationSchema = new mongoose.Schema(
  {
    user_id: {
      type: type.ObjectId,
      ref: 'users',
      required: [
        true,
        'Vous devez vous inscrire sur le site pour bénéficier de ce service.',
      ],
    },
    location_id: { type: type.ObjectId, ref: 'locations' },
    year: Number,
    month: Number,
    day: Number,
    start: Number,
    end: Number,
    price: { type: Number, default: 0 },
    fidelity_points: { type: Number, default: 0 },
    currency: { type: String, default: 'euro' },
  },
  { timestamps: true },
);

// Modèle
module.exports = mongoose.model('reservations', reservationSchema);

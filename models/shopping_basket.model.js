const mongoose = require('mongoose');
const moment = require('moment');
const types = mongoose.Types;

// Schéma
const shoppingBasketSchema = new mongoose.Schema(
  {
    articles: [
      {
        product_id: { type: types.ObjectId, ref: 'products' },
        option_ids: [{ type: types.ObjectId, ref: 'options', default: [] }],
        quantity: { type: Number, default: 1 },
        price: { type: Number, default: 0 },
      },
    ],
    reservations: [
      {
        reservation_id: {
          type: types.ObjectId,
          ref: 'reservations',
        },
        price: { type: Number, default: 0 },
      },
    ],
    total_price: Number,
    user_id: { type: types.ObjectId, ref: 'users', default: null },
    preview_fidelity_points: { type: Number, default: 0 },
  },
  { timestamps: true },
);

// Modèle
module.exports = mongoose.model('shopping_baskets', shoppingBasketSchema);

const mongoose = require('mongoose');
const moment = require('moment');
const type = mongoose.Types;

// Schéma
const LocationCategorySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'nom de la pièce manquant'],
    },
    stuff: {
      type: String,
      default: "Pas d'équipements.",
    },
    currency: {
      type: String,
      default: 'euro',
    },
    price: { type: Number, default: 0 },
    place_number: {
      type: Number,
      default: 1,
    },
    points: { type: Number, default: 0 },
    offer: { type: Number, default: 0 },
  },
  { timestamps: true },
);

// Modèle
module.exports = mongoose.model('location_categories', LocationCategorySchema);

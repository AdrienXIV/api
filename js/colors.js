const chalk = require('chalk');

// COULEURS CONSOLE
const colors = {
  bold: {
    blue: chalk.bold.hex('#0652DD'),
    yellow: chalk.bold.yellow,
    red: chalk.bold.hex('#EA2027'),
    magenta: chalk.bold.magenta,
    green: chalk.bold.hex('#009432'),
    orange: chalk.bold.hex('#d35400'),
  },
  yellow: chalk.yellow,
  blue: chalk.blue,
};

module.exports = colors;

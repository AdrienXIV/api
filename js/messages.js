const color = require('./colors');

// afficher des messages dans la console
exports.message = text => {
  console.log(color.blue(text));
};

// afficher des messages de succès
exports.success = text => {
  console.log(color.bold.green(text));
};

// afficher les messages d'erreurs
// TODO: enregistrer les messages d'érreurs en log
exports.error = text => {
  console.error(color.bold.red(text));
};

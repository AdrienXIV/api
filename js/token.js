const jwt = require('jsonwebtoken');

// MESSAGES CONSOLE
const message = require('./messages');

const verifyUser = ['client'];
const verifyEmployee = ['admin', 'employee', 'manager'];

exports.checkUserToken = (req, res, next) => {
  try {
    let decoded = jwt.verify(
      req.headers.authorization.split(' ')[1],
      process.env.SECRET_KEY,
    );
    // vérifier que le role soit client pour accéder aux fonctionnalités des personnes inscrites
    decoded ? next() : res.sendStatus(401);
  } catch (error) {
    message.error('checkUserToken catch : ' + error);
    // si le token ne correspond pas, le catch apparaît
    res.sendStatus(401);
  }
};

exports.checkEmployeeToken = (req, res, next) => {
  // si le token existe
  if (req.headers.authorization)
    try {
      let decoded = jwt.verify(
        req.headers.authorization.split(' ')[1],
        process.env.SECRET_KEY,
      );
      // vérifier que le role soit employee, admin ou manager pour accéder au backoffice
      verifyEmployee.indexOf(decoded.role) >= 0 ? next() : res.sendStatus(401);
    } catch (error) {
      message.error('checkEmployeeToken catch : ' + error);
      // si le token ne correspond pas, le catch apparaît
      res.sendStatus(401);
    }
  else res.sendStatus(401);
};

exports.checkSocketToken = token => {
  try {
    let decoded = jwt.verify(token, process.env.SECRET_KEY);
    // vérifier que le role soit employee ou admin pour accéder au backoffice
    return decoded ? true : false;
  } catch (error) {
    //message.error('checkSocketEmployeeToken catch : ' + error);
    // si le token ne correspond pas, le catch apparaît
    return false;
  }
};

exports.checkRoleToken = (req, res) => {
  // si le token existe
  if (req.headers.authorization)
    try {
      let decoded = jwt.verify(
        req.headers.authorization.split(' ')[1],
        process.env.SECRET_KEY,
      );
      // vérifier que le role soit employee ou admin pour accéder au backoffice
      return decoded ? decoded.role : res.sendStatus(401);
    } catch (error) {
      message.error('checkRoleToken catch : ' + error);
      // si le token ne correspond pas, le catch apparaît
      res.sendStatus(401);
    }
  else res.sendStatus(401);
};

/**
 * Création d'un jeton d'authentification
 * @param {String} id - MongoDB _id
 * @param {String} role - MongoDB role
 * @returns token
 */
exports.createToken = (id, role, firstname, lastname, email) => {
  try {
    return jwt.sign(
      {
        sub: String(
          Number(Date.now()) *
            Math.exp(Math.exp(Math.LN10)) *
            Math.exp(Math.PI),
        ),
        userId: id,
        role,
        firstname,
        lastname,
        email,
        expiresIn: '24h',
      },
      process.env.SECRET_KEY,
      {
        algorithm: 'HS384',
      },
    );
  } catch {
    return false;
  }
};

exports.getUserInfos = (req, res) => {
  // si le token existe
  if (req.headers.authorization)
    try {
      let decoded = jwt.verify(
        req.headers.authorization.split(' ')[1],
        process.env.SECRET_KEY,
      );
      return decoded;
    } catch (error) {
      message.error('getUserInfos catch : ' + error);
      // si le token ne correspond pas ou est malformé
      return false;
    }
  else return false;
};

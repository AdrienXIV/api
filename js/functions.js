// modèles
const ShoppingBasket = require('../models/shopping_basket.model');
const Order = require('../models/order.model');
const Product = require('../models/product.model');
const Option = require('../models/option.model');
const User = require('../models/user.model');
const NewsLetter = require('../models/newsletter.model');
const randomstring = require('randomstring');
const moment = require('moment');

const transport = require('./sendMail');

// MESSAGES CONSOLE
const message = require('./messages');

const jwt = require('./token');

/**
 * @params {String} string
 * @returns {String} test => Test
 */
exports.capitalizeFirstLetter = string => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

/**
 * Trier les dates des commandes
 * @param {String} param
 * @returns {Object} objet contenant l'intervalle de début et de fin
 */
exports.sortDate = param => {
  const today = moment().startOf('day');
  switch (param) {
    case 'today':
      return {
        start: today.toDate(),
        end: moment(today).endOf('day').toDate(),
      };
    case 'week':
      return {
        start: moment().startOf('week').toDate(),
        end: moment().endOf('week').toDate(),
      };
    case 'month':
      return {
        start: moment().startOf('month').toDate(),
        end: moment().endOf('month').toDate(),
      };
    case 'year':
      return {
        start: moment().startOf('year').toDate(),
        end: moment().endOf('year').toDate(),
      };
    default:
      return {
        start: today.toDate(),
        end: moment(today).endOf('day').toDate(),
      };
  }
};

exports.createRandomString = () => {
  return randomstring.generate({
    length: 6,
    charset: 'alphanumeric',
    capitalization: 'uppercase',
  });
};

// ############################################################################
//
// Fonctions du controlleur préparant la requête vers la base de données
// afin d'éviter la répétition de code et d'avoir une meilleure visibilité
//
// ############################################################################

/**
 * Récupérer le panier avec toutes les références
 * @param {String} _id - req.params.id
 * @returns {Promise} ShoppingBasket.findById()
 */
exports.findBasket = _id => {
  return ShoppingBasket.findById(_id)
    .populate({
      path: 'articles.product_id',
      model: 'products',
      populate: {
        path: 'category_id',
        model: 'categories',
      },
    })
    .populate({ path: 'articles.option_ids', model: 'options' })
    .populate({
      path: 'reservations.reservation_id',
      model: 'reservations',
      populate: {
        path: 'location_id',
        model: 'locations',
        populate: {
          path: 'location_category_id',
          model: 'location_categories',
        },
      },
    });
};

/**
 * Récupérer le panier avec l'id de l'utilisateur dans son token
 * @param {String} user_id - token.userId
 * @returns {Promise} ShoppingBasket.findOne()
 */
exports.findBasketWithUserId = user_id => {
  return ShoppingBasket.findOne({ user_id })
    .populate({
      path: 'articles.product_id',
      model: 'products',
      populate: {
        path: 'category_id',
        model: 'categories',
      },
    })
    .populate({ path: 'articles.option_ids', model: 'options' });
};

/**
 * Récupérer la commande
 * @param {String} _id - req.params.id
 * @returns {Promise} Order.findById()
 */
exports.findOrder = _id => {
  return Order.findById(_id)
    .populate({
      path: 'shopping_basket_id',
      populate: [
        {
          path: 'articles.option_ids',
          model: 'options',
        },
        {
          path: 'articles.product_id',
          model: 'products',
          populate: {
            path: 'category_id',
            model: 'categories',
          },
        },
        {
          path: 'reservations.reservation_id',
          model: 'reservations',
          populate: {
            path: 'location_id',
            model: 'locations',
            populate: {
              path: 'location_category_id',
              model: 'location_categories',
            },
          },
        },
      ],
    })
    .populate('user_id');
};

/**
 * Récupérer toutes les commandes d'un utilisateur inscrit de la plus récente à la plus vieille
 * @param {String} user_id - req.params.user_id
 * @returns {Promise} Order.find()
 */
exports.findUserOrders = user_id => {
  return Order.find({ user_id }, null, { sort: { createdAt: -1 } })
    .populate({
      path: 'shopping_basket_id',
      populate: [
        {
          path: 'articles.option_ids',
          model: 'options',
        },
        {
          path: 'articles.product_id',
          model: 'products',
          populate: {
            path: 'category_id',
            model: 'categories',
          },
        },
      ],
    })
    .populate('user_id');
};

/**
 * Mise à jour des points de fidélité
 * Vérification si l'utilisateur est inscrit en BDD avant de mettre à jour
 * @param {String} user_id
 * @param {Number} points
 * @returns {Void}
 */
exports.updateFidelityPoints = (user_id, points) => {
  // si l'utilisateur est un client enregistré
  // on met à jour ses points de fidélité

  User.findById(user_id)
    .then(user => {
      // si l'utilisateur est bien en BDD
      if (user) {
        const fidelity_points = user.fidelity_points + points;
        // mise à jour de ses points
        return user
          .set({
            fidelity_points,
          })
          .save();
      }
    })
    .then(() => {
      //TODO: envoyer un mail avec le récapitulatif de la commande
      return;
    })
    .catch(error => {
      message.error('functions/updateFidelityPoints : ' + error);
    });
};

// ##################################
//              MAIL
// ##################################
exports.sendMailWhenOrderIsPaid = (_id, email) => {
  moment.locale('fr');
  this.findOrder(_id)
    .then(order => {
      let body = '';

      body += '<div style="width:100%; background-color:white">';
      body += `<h2>Votre facture pour la commande ${
        order.id_order
      } passée le ${moment(order.createdAt).format('LLLL')}</h2>`;

      if (order.shopping_basket_id.articles.length > 0) {
        body += '<h4 style="text-decoration: underline;">Articles :</h4>';
        body += '<table style="width:100%; border:solid 1px black;">';
        body += '<tr>';
        body += '<th style="width:25%;">article</th>';
        body += '<th style="width:25%;">options</th>';
        body += '<th style="width:25%;">quantité</th>';
        body += '<th style="width:25%;">prix</th>';
        body += '</tr>';
        order.shopping_basket_id.articles.forEach(article => {
          body += '<tr>';
          // produit
          body += `<td style="text-align:'center';">${article.product_id.name}</td>`;
          // options
          body += `<td style="text-align:'center';">`;
          if (article.option_ids)
            article.option_ids.forEach(option => {
              body += `${option.name}<br>`;
            });
          body += '</td>';
          // quantité
          body += `<td style="text-align:'center';">${article.quantity}</td>`;
          // prix
          body += `<td style="text-align:'center';">${article.price}€</td>`;
          body += '</tr>';
        });
        body += '</table>';
      }

      if (order.shopping_basket_id.reservations.length > 0) {
        body +=
          '<h4 style="text-decoration: underline;">Réservation des espaces de travail :</h4>';
        body += '<table style="width:100%; border:solid 1px black;">';
        body += '<tr>';
        body += '<th style="width:25%;">espace de travail</th>';
        body += '<th style="width:25%;">début</th>';
        body += '<th style="width:25%;">fin</th>';
        body += '<th style="width:25%;">prix</th>';
        body += '</tr>';

        order.shopping_basket_id.reservations.forEach(reservation => {
          body += '<tr>';
          // espace
          body += `<td style="text-align:'center';">${reservation.reservation_id.location_id.name}</td>`;
          // début
          body += `<td style="text-align:'center';">Début : ${new Date(
            reservation.reservation_id.year,
            reservation.reservation_id.month,
            reservation.reservation_id.day,
          ).toLocaleDateString()} à ${reservation.reservation_id.start}h</td>`;
          body += `<td style="text-align:'center';">Fin : ${new Date(
            reservation.reservation_id.year,
            reservation.reservation_id.month,
            reservation.reservation_id.day,
          ).toLocaleDateString()} à ${reservation.reservation_id.end}h</td>`;
          // prix
          body += `<td style="text-align:'center';">${reservation.price}€</td>`;
          body += '</tr>';
        });
        body += '</table>';
      }

      body += "<div style='margin-top:50px;width:100%;text-align:right;'>";
      body += `<p>Montant Total TTC : ${order.price}€</p>`;
      body += `<p>TVA 20% : ${(order.price * 0.2).toFixed(2)}€</p>`;
      body += '</div>';
      body += '</div>';

      const mailMessage = {
        from: '"Gusto Coffee" <nws.gustocoffee@gmail.com>', // sender address
        to: email, // list of receivers
        subject: `Votre commande Gusto Coffee`, // Subject line
        html: body, // html body
      };
      // envoie du mail
      transport.sendMail(mailMessage, function (error) {
        if (error) {
          message.error('sendMailWhenOrderIsPaid : ' + error.message);
          return;
        }
        // if you don't want to use this transport object anymore, uncomment following line
        transport.close(); // close the connection pool
      });
    })
    .catch(error => {
      message.error('sendMailWhenOrderIsPaid findOrder : ' + error);
    });
};

exports.sendMailWhenUserIsRegistered = (email, firstname, lastname) => {
  let body = `<h2>Bienvenue ${firstname} ${lastname} sur Gusto Coffee !</h2>`;
  const mailMessage = {
    from: '"Gusto Coffee" <nws.gustocoffee@gmail.com>', // sender address
    to: email, // list of receivers
    subject: `Inscription réussi !`, // Subject line
    html: body, // html body
  };

  // envoie du mail
  transport.sendMail(mailMessage, function (error) {
    if (error) {
      message.error('sendMailWhenUserIsRegistered : ' + error.message);
      return;
    }
  });
  // if you don't want to use this transport object anymore, uncomment following line
  transport.close(); // close the connection pool
};

exports.sendMailWhenUserForgotHisPassword = (email, link, res) => {
  let body = `<h2>Cliquez sur le lien pour réinitialiser votre mot de passe (expire dans 15 min)</h2>`;
  body += `<p><strong>${link}</strong></p>`;
  const mailMessage = {
    from: '"Gusto Coffee" <nws.gustocoffee@gmail.com>', // sender address
    to: email, // list of receivers
    subject: `Réinitialisation de votre mot de passe`, // Subject line
    html: body, // html body
  };
  // envoie du mail
  transport.sendMail(mailMessage, function (error) {
    if (error) {
      message.error('sendMailWhenUserIsRegistered : ' + error.message);
      return;
    }
    // if you don't want to use this transport object anymore, uncomment following line
    transport.close(); // close the connection pool
    res.sendStatus(200);
  });
};

// inscripiption à la newsletter
exports.newslettersSignup = (req, res, next) => {
  NewsLetter.findOne({ email: req.body.email })
    .then(newsletter => {
      if (newsletter) throw { code: 400 };
      // si l'email n'existe pas on l'inscrit
      return new NewsLetter({ email: req.body.email }).save();
    })
    .then(newsletter => {
      let body = `<h2>Vous venez de vous inscrire à notre Newsletter</h2>`;
      body += `<p>Nous vous enverrons régulièrement des offres promotionnelles que propose Gusto Coffee.</p>`;
      const mailMessage = {
        from: '"Gusto Coffee" <nws.gustocoffee@gmail.com>', // sender address
        to: newsletter.email, // list of receivers
        subject: `Newsletter Gusto Coffee`, // Subject line
        html: body, // html body
      };
      // envoie du mail
      transport.sendMail(mailMessage, function (error) {
        if (error) {
          message.error('newslettersSignup : ' + error.message);
          return;
        }
        // if you don't want to use this transport object anymore, uncomment following line
        transport.close(); // close the connection pool
        res.sendStatus(201);
      });
    })
    .catch(error => {
      if (error.code === 400)
        res.status(400).json({ error: 'Courriel déjà existant' });
      else res.status(500).json({ error: 'Erreur serveur' });
    });
};

const express = require('express');
const router = express.Router();

// Modèle
const Order = require('../models/order.model');

// controller
const orderController = require('../controllers/order.controller');

// MESSAGES CONSOLE
const message = require('../js/messages');

var expiryDate = new Date(Date.now() + 60 * 60 * 24 * 1000); // 24 hour

/**
 * GET
 */
// récupérer les commandes de l'utilisateur
router.get('/', orderController.getUserOrders);

// récupérer la commande une fois que le panier a été confirmé
router.get('/:id', orderController.getOrder);

/**
 * POST
 */
// si le paiement est validé (banque, paypal, etc), une requête est envoyée afin que la commande soit préparée
router.post('/payment', orderController.payment);

// mobile
router.post('/mobile/stripe-payment', orderController.mobileCreatePayment);

module.exports = router;

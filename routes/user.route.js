const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller');

// Validation des champs
const { check, validationResult } = require('express-validator');
// MESSAGES CONSOLE
const message = require('../js/messages');

var expiryDate = new Date(Date.now() + 60 * 60 * 24 * 1000); // 24 hour
// champs de validation pour la modification (en option car tous les champs ne sont pas forcément envoyés)
const editValidation = [
  check('firstname').isString().optional(),
  check('lastname').isString().optional(),
  check('email').isEmail().withMessage('Courriel invalide.').optional(),
  check('password')
    .isLength({
      min: 5,
    })
    .withMessage('Le mot de passe doit contenir au moins 5 caractères.')
    .optional(),
];

/**
 * PATCH
 */

// modification profil
router.patch('/', editValidation, (req, res) => {
  let errors = validationResult(req);
  !errors.isEmpty()
    ? // s'il y a des erreurs
      res.status(400).json({ error: errors.array() })
    : userController.editUser(req, res);
});

// modification du mot de passe de l'utilisateur
router.patch('/edit-password', editValidation, (req, res) => {
  let errors = validationResult(req);
  !errors.isEmpty()
    ? // s'il y a des erreurs
      res.status(400).json({ error: errors.array() })
    : userController.editUserPassword(req, res);
});

/**
 * DELETE
 */
// suppression profil
router.delete('/', userController.deleteUser);

module.exports = router;

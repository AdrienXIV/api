const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller');

// Validation des champs
const { check, validationResult } = require('express-validator');

// champs de validation pour la création
//TODO: vérification pour les images avatar
const signupValidation = [
  check('firstname').isString(),
  check('lastname').isString(),
  check('email').isEmail().withMessage('Courriel invalide.'),

  check('password')
    .isLength({ min: 10 })
    .withMessage('Le mot de passe doit contenir au moins 10 caractères.')
    .matches('[0-9]')
    .withMessage('Le mot de passe doit contenir au moins un chiffre.')
    .matches('[a-z]')
    .withMessage('Le mot de passe doit contenir au moins une lettre minuscule.')
    .matches('[A-Z]')
    .withMessage(
      'Le mot de passe doit contenir au moins une lettre majuscule.',
    ),
];

const passwordValidation = [
  check('password')
    .isLength({ min: 10 })
    .withMessage('Le mot de passe doit contenir au moins 10 caractères.')
    .matches('[0-9]')
    .withMessage('Le mot de passe doit contenir au moins un chiffre.')
    .matches('[a-z]')
    .withMessage('Le mot de passe doit contenir au moins une lettre minuscule.')
    .matches('[A-Z]')
    .withMessage(
      'Le mot de passe doit contenir au moins une lettre majuscule.',
    ),
];

// champs de validation pour la modification (en option car tous les champs ne sont pas forcément envoyés)
const editValidation = [
  check('firstname').isString().optional(),
  check('lastname').isString().optional(),
  check('email').isEmail().withMessage('Courriel invalide.').optional(),
  check('password')
    .isLength({ min: 10 })
    .withMessage('Le mot de passe doit contenir au moins 10 caractères.')
    .matches('[0-9]')
    .withMessage('Le mot de passe doit contenir au moins un chiffre.')
    .matches('[a-z]')
    .withMessage('Le mot de passe doit contenir au moins une lettre minuscule.')
    .matches('[A-Z]')
    .withMessage(
      'Le mot de passe doit contenir au moins une lettre majuscule.',
    ),
];

/**
 * GET
 */
router.get('/', userController.getUsers);

router.get('/profile', userController.getProfile);

/**
 * POST
 */
// inscription
router.post('/signup', userController.signup);

// connexion
router.post('/login', userController.login);

router.post('/forgot-password', userController.forgotPassword);

router.post('/reset-password', passwordValidation, (req, res) => {
  let errors = validationResult(req);
  !errors.isEmpty()
    ? // s'il y a des erreurs
      res.status(400).send(errors.array())
    : userController.resetPassword(req, res);
});

module.exports = router;

const express = require('express');
const router = express.Router();

// controller
const optionController = require('../controllers/option.controller');

/**
 * GET
 */
router.get('/', optionController.getOptions);
router.get('/:id', optionController.getOption);

module.exports = router;

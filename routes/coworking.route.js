const express = require('express');
const router = express.Router();

// Validation des champs
const { check, validationResult } = require('express-validator');
// controller
const coworkingController = require('../controllers/coworking.controller');

// MESSAGES CONSOLE
const message = require('../js/messages');

var expiryDate = new Date(Date.now() + 60 * 60 * 24 * 1000); // 24 hour

/**
 * GET
 */
router.get('/', coworkingController.getLocationCategories);
router.get('/location', coworkingController.getLocations);

router.get('/:id', coworkingController.getLocationCategory);
router.get('/location/:id', coworkingController.getLocation);

router.get('/:id/location', coworkingController.getCategoryLocations);

module.exports = router;

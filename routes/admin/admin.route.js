const express = require('express');
const router = express.Router();

// Validation des champs
const { check, validationResult } = require('express-validator');
// controller
const adminController = require('../../controllers/admin/admin.controller');

// MESSAGES CONSOLE
const message = require('../../js/messages');

var expiryDate = new Date(Date.now() + 60 * 60 * 24 * 1000); // 24 hour

// champs de validation pour la création
//TODO: vérification pour les images avatar
const signupValidation = [
  check('firstname').isString(),
  check('lastname').isString(),
  check('role').isString(),
  check('email').isEmail().withMessage('Courriel invalide.'),
  check('password')
    .isLength({
      min: 5,
    })
    .withMessage('Le mot de passe doit contenir au moins 5 caractères.'),
];

/**
 * GET
 */
router.get('/', function (req, res) {
  res.sendStatus(200);
});

router.get('/check-role', adminController.checkRole);

// récupérer tous les utilisateurs dans le backoffice avec leurs infos
// route protégée dont seul les admins peuvent accéder à ces infos concernants les utilisateurs
router.get('/users', adminController.getUsers);
router.get('/user/:id', adminController.getUser);

/**
 * POST
 */
// employés inscription
router.post('/signup', signupValidation, (req, res) => {
  let errors = validationResult(req);
  !errors.isEmpty()
    ? // s'il y a des erreurs
      res.json({ status: 400, error: errors.array() })
    : adminController.signup(req, res);
});

/**
 * PATCH
 */
router.patch('/user/:id', adminController.editUser);
router.patch('/user/:id/edit-password', adminController.editUserPassword);

/**
 * DELETE
 */
router.delete('/user/:id', adminController.deleteUser);
module.exports = router;

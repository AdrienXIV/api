const express = require('express');
const router = express.Router();

// controller
const orderController = require('../../controllers/admin/order.admin.controller');

// MESSAGES CONSOLE
const message = require('../../js/messages');

var expiryDate = new Date(Date.now() + 60 * 60 * 24 * 1000); // 24 hour

/**
 * GET
 */
router.get('/', orderController.getOrders);
router.get('/paid', orderController.getOrdersPaid);
router.get('/history', orderController.getOrderHistoryDelivered);
router.get('/prepared', orderController.getOrdersPrepared);
router.get('/:id', orderController.getOrder);
/**
 * POST
 */

/**
 * PATCH
 */

router.patch('/:id', orderController.updateOrderStatus);
/**
 * DELETE
 */
module.exports = router;

const express = require('express');
const router = express.Router();

// Validation des champs
const { check, validationResult } = require('express-validator');
// controller
const categoryController = require('../../controllers/admin/category.admin.controller');

// MESSAGES CONSOLE
const message = require('../../js/messages');

var expiryDate = new Date(Date.now() + 60 * 60 * 24 * 1000); // 24 hour

/**
 * POST
 */
router.post('/new', categoryController.newCategory);

/**
 * PATCH
 */
router.patch('/:id', categoryController.editCategory);

/**
 * DELETE
 */
router.delete('/:id', categoryController.deleteCategory);

module.exports = router;

const assert = require('assert');
const expect = require('chai').expect;
const should = require('chai').should();
const axios = require('axios');

const API_URL = 'http://localhost:3000';
const user = {
  lastname: 'Nom',
  firstname: 'Prénom',
  email: 'nom.prenom@test.test',
  password: 'Inscriptionutilisateur123!',
};
const failUser = {
  lastname: '',
  firstname: '',
  email: 'test@@test',
  password: 'azertyuiop123',
};

const register = async data => {
  try {
    const response = await axios.post(`${API_URL}/auth/signup`, data);
    return response;
  } catch (error) {
    return error.response;
  }
};
const login = async data => {
  try {
    const response = await axios.post(`${API_URL}/auth/login`, data);
    return response;
  } catch (error) {
    return error.response;
  }
};

/**
 * INSCRIPTION
 */
let token = '';
let headers = {};
let profile = {};

describe('Inscription', () => {
  it('Echec - 409 - champs mal remplis', async () => {
    const { data } = await register(failUser);
    expect(data.error).to.equal(
      'Prénom manquant | Nom manquant | Format inccorect pour le courriel | Format incorrect pour le mot de passe',
    );
  });
  it('succès - 201', async () => {
    const { status } = await register(user);
    expect(status).to.equal(201);
  });
  it('erreur - 400 - courriel déjà existant', async () => {
    const { status } = await register(user);
    expect(status).to.equal(400);
  });
});

describe('Connexion', () => {
  it('Echec - 409 - champs mal remplis', async () => {
    const { data } = await login({ password: user.password });
    expect(data.error).to.equal('Veuillez renseigner un courriel');
  });
  it('succès - 200', async () => {
    const { status, data } = await login(user);
    token = data.user.token;
    headers = {
      authorization: `Baerer ${token}`,
    };
    expect(data.user.email).to.equal(user.email);
  });
});

describe("Processus d'achat", () => {
  const articles = [
    {
      product_id: '5ebd3bbfa6f5d67e0d1baf1d',
      option_ids: [],
      quantity: 2,
      price: 6,
    },
    {
      product_id: '5ebe56eb2e4734d076053251',
      option_ids: [],
      quantity: 1,
      price: 5,
    },
  ];
  let basket_response = null;
  it('Achat', async () => {
    basket_response = await axios.post(
      `${API_URL}/shopping-basket/confirm`,
      {
        articles,
        reservations: [],
        total_price: 11,
        preview_fidelity_points: 11,
      },
      {
        headers,
      },
    );
    should.exist(basket_response.data._id);
  });

  it('Echec paiement', async () => {
    try {
      const { status } = await axios.post(
        `${API_URL}/order/payment`,
        {
          _id: basket_response.data._id,
          id_stripe: 'pm_1IPbLU2eZvKYlo2CBLU7oRUJ',
          currency: 'euro',
          payment_method: 'bank card',
        },
        {
          headers,
        },
      );
    } catch (error) {
      expect(error.response.data.error).to.equal(
        'Problème survenu lors du paiement',
      );
    }
  });
});

describe('Profil', () => {
  it('Récupération du profil', async () => {
    const { data } = await axios.get(`${API_URL}/auth/profile`, { headers });
    profile = data;

    expect(data.email).to.equal(user.email);
  });
});

describe('Suppression compte', () => {
  it('succès - 200', async () => {
    const { status } = await axios.delete(`${API_URL}/user`, { headers });
    expect(status).to.equal(200);
  });
});
